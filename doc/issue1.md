## 问题现象

执行程序时，出现如下打印，不显示窗口，并且程序无法自动结束。

![image-20200403105411077](issue1.assets/image-20200403105411077.png)

## 定位过程

1. 观察程序输出，发现有qt5ct插件的打印信息，还有GTK的打印信息。GTK是一个图形编程库，与QT没有关系，因此怀疑程序的打印应该与qt5ct这个插件有关系。

2. 查询软件包：https://www.debian.org/distrib/packages，Raspbian为了让所有的图形应用外观看起来一致，因此增加此软件包。

3. 使用strace跟踪openat系统调用，可以发现程序读取/etc/xdg/qt5ct下的配置文件。查看此配置文件，发现使用了GTK2的主题，说明程序会加载GTK的库，从而导致错误。

4. 上网查询如何禁用qt5ct插件，发现修改QT_QPA_PLATFORMTHEME环境变量可以禁用此插件。
5. 删除此环境变量，问题解决。

## 问题原因

Raspbian为了保证图形应用程序界面的一致性，使用qt5ct插件，此插件会加载gtk的库，opencv部分函数也链接了不同版本的gtk库，同一个应用程序中存在不同版本的gtk函数库，造成gtk函数库初始化失败，因此导致qt的应用程序不能正常运行。

## 解决方法

禁用qt5ct插件，删除QT_QPA_PLATFORMTHEME环境变量。