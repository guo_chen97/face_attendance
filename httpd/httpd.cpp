#include <cstdio>
#include <cstdlib> //malloc
#include <cstring> //memset
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h> //sockaddr_in
#include <arpa/inet.h>  //inet_ntop
#include <signal.h>
#include <pthread.h>
#include <unistd.h> //sleep
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

VideoCapture cam;

Mat frame;
pthread_mutex_t frame_mutex = PTHREAD_MUTEX_INITIALIZER;

Rect range;
pthread_mutex_t range_mutex = PTHREAD_MUTEX_INITIALIZER;

void *handle_client(void *arg)
{
    char buf[BUFSIZ];

    FILE *sfp = fdopen((long)arg, "r+");
    if (!sfp)
    {
        perror("fdopen");
        return NULL;
    }

    while (1)
    {
        while (fgets(buf, BUFSIZ, sfp) && strlen(buf) > 2)
        {
            //printf(buf);
        }

        if (ferror(sfp) || feof(sfp))
        {
            printf("connection closed\n");
            break;
        }

        Mat image;
        vector<unsigned char> body;
        pthread_mutex_lock(&frame_mutex);
        image = frame;
        pthread_mutex_unlock(&frame_mutex);

        pthread_mutex_lock(&range_mutex);
        rectangle(image, range, CV_RGB(255, 0, 0));
        pthread_mutex_unlock(&range_mutex);

        imencode(".jpg", image, body);

        fprintf(sfp, "HTTP/1.1 200 OK\r\n");
        fprintf(sfp, "Content-Type: image/jpeg\r\n");
        fprintf(sfp, "Content-Length: %ld\r\n", body.size());
        fprintf(sfp, "\r\n");
        fwrite((char *)body.data(), 1, body.size(), sfp);
    }

    fclose(sfp);

    return NULL;
}

void *capture(void *)
{
    puts("camera start");

    while (1)
    {
        pthread_mutex_lock(&frame_mutex);
        cam >> frame;
        pthread_mutex_unlock(&frame_mutex);
        usleep(20000); //50 fps
    }
}

void *detector(void *)
{
    CascadeClassifier classifier;
    //classifier.load("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");
    classifier.load("/usr/share/opencv/lbpcascades/lbpcascade_frontalface.xml");
    Mat gray;
    vector<Rect> faces;

    puts("face detection start");

    while (1)
    {
        cvtColor(frame, gray, COLOR_BGR2GRAY);
        equalizeHist(gray, gray);
        classifier.detectMultiScale(gray, faces, 2, 1);
        if (faces.size())
        {
            pthread_mutex_lock(&range_mutex);
            range = faces[0];
            pthread_mutex_unlock(&range_mutex);
        }
    }
}

int main()
{
    pthread_attr_t attr;

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    cam.open(0);
    cam >> frame; //make sure frame is not empty

    pthread_t capture_tid;
    pthread_t detector_tid;
    pthread_create(&capture_tid, &attr, capture, NULL);
    pthread_create(&detector_tid, &attr, detector, NULL);

    signal(SIGPIPE, SIG_IGN);

    int listenfd = socket(AF_INET, SOCK_STREAM, 0);

    if (listenfd < 0)
    {
        perror("socket");
        return 1;
    }

    int opt = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    struct sockaddr_in saddr;
    memset(&saddr, 0, sizeof(saddr));

    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(8000);
    if (bind(listenfd, (struct sockaddr *)&saddr, sizeof(saddr)) < 0)
    {
        perror("bind");
        return 1;
    }

    if (listen(listenfd, 64) < 0)
    {
        perror("listen");
        return 1;
    }

    puts("web server start");

    while (1)
    {
        struct sockaddr_in caddr;
        socklen_t addrlen = sizeof(caddr);
        long connfd;
        connfd = accept(listenfd, (struct sockaddr *)&caddr, &addrlen);

        char ipstr[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &caddr.sin_addr, ipstr, sizeof ipstr);
        printf("client %s connected\n", ipstr);
        if (connfd < 0)
        {
            perror("accept");
            continue;
        }

        pthread_t tid;
        pthread_create(&tid, NULL, handle_client, (void *)connfd);
    }
}
