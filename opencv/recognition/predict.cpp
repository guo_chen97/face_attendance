#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/face/facerec.hpp>

using namespace std;
using namespace cv;

string name[] = {"KA", "KL", "KM", "KR", "MK", "NA", "NM", "TM", "UY", "YM"};

int main(int argc, char** argv)
{
    Mat image = imread(argv[1], IMREAD_GRAYSCALE);

    auto recognizer1 = face::createEigenFaceRecognizer();
    recognizer1->load("name1.xml");
    cout << name[recognizer1->predict(image)] << endl;

    auto recognizer2 = face::createFisherFaceRecognizer();
    recognizer2->load("name2.xml");
    cout << name[recognizer2->predict(image)] << endl;

    auto recognizer3 = face::createLBPHFaceRecognizer();
    recognizer3->load("name3.xml");
    cout << name[recognizer3->predict(image)] << endl;

    return 0;
}
