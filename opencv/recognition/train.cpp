#include <iostream>
#include <fstream>
#include <map>
#include <opencv2/opencv.hpp>
#include <opencv2/face/facerec.hpp>

using namespace std;
using namespace cv;

vector<Mat> images;
vector<int> labels;
map<string, int> name;

int load_image(string& file)
{
    images.push_back(imread(file, IMREAD_GRAYSCALE));
    labels.push_back(name[file.substr(0, 2)]);
    return 0;
}

int main()
{
    name["KA"] = 0;
    name["KL"] = 1;
    name["KM"] = 2;
    name["KR"] = 3;
    name["MK"] = 4;
    name["NA"] = 5;
    name["NM"] = 6;
    name["TM"] = 7;
    name["UY"] = 7;
    name["YM"] = 7;
    
    ifstream filelist("list.txt");
    string file;
    while(getline(filelist, file))
    {
        //cout << file << endl;
        load_image(file);
    }

    auto recognizer1 = face::createEigenFaceRecognizer();
    recognizer1->train(images, labels);
    recognizer1->save("name1.xml");

    auto recognizer2 = face::createFisherFaceRecognizer();
    recognizer2->train(images, labels);
    recognizer2->save("name2.xml");

    auto recognizer3 = face::createLBPHFaceRecognizer();
    recognizer3->train(images, labels);
    recognizer3->save("name3.xml");

    return 0;
}
